from django.db import models as m


class User(m.Model):
    user_id = m.BigIntegerField(null=False, unique=True, blank=True)
    count_of_created_tasks = m.IntegerField()
    check_box = m.BooleanField(default=False)


class Task(m.Model):
    user_id = m.BigIntegerField(null=False, blank=True)
    title = m.CharField(max_length=60, null=False, blank=True)
    description = m.CharField(max_length=255, null=True)


class UserTask(m.Model):
    user_id = m.BigIntegerField(null=False, blank=True)
    title = m.CharField(max_length=60, null=True, blank=True)
    description = m.CharField(max_length=255, null=True)
    state = m.CharField(max_length=30, null=True, blank=True)


class TextMessage(m.Model):
    message_key = m.CharField(max_length=64, unique=True, null=False, blank=True)
    message_text = m.TextField(max_length=128, null=False, blank=True)


class State(m.Model):
    state_key = m.CharField(max_length=64, unique=True, null=False, blank=True)
    state_value = m.CharField(max_length=64, unique=True, null=False, blank=True)


class HandleCommand(m.Model):
    command_key = m.CharField(max_length=64, unique=True, null=False, blank=True)
    command_value = m.CharField(max_length=64, unique=True, null=False, blank=True)


class TextKeyboard(m.Model):
    kb_key = m.CharField(max_length=64, unique=True, null=False, blank=True)
    kb_text = m.CharField(max_length=64, unique=True, null=False, blank=True)