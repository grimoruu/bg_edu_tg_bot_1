from django.contrib import admin

# Register your models here.

from .models import *


@admin.register(User)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'count_of_created_tasks', 'check_box',)
    list_display_links = ('id', 'user_id', 'count_of_created_tasks',)
    search_fields = ('id', 'user_id', 'count_of_created_tasks',)
    list_filter = ('check_box', 'count_of_created_tasks')
    list_editable = ('check_box',)
    save_as = True


@admin.register(Task)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'title', 'description',)
    list_display_links = ('id', 'user_id', 'title', 'description',)
    search_fields = ('id', 'user_id', 'title',)
    save_as = True


@admin.register(UserTask)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'title', 'description', 'state',)
    list_display_links = ('id', 'user_id', 'title', 'description', 'state',)
    search_fields = ('id', 'user_id', 'state',)
    save_as = True


@admin.register(TextMessage)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'message_key', 'message_text',)
    list_display_links = ('id', 'message_text',)
    search_fields = ('id', 'message_key', 'message_text',)
    save_as = True


@admin.register(State)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'state_key', 'state_value',)
    list_display_links = ('id', 'state_key', 'state_value',)
    search_fields = ('id', 'state_key', 'state_value',)
    save_as = True


@admin.register(HandleCommand)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'command_key', 'command_value',)
    list_display_links = ('id', 'command_key', 'command_value',)
    search_fields = ('id', 'command_key', 'command_value',)
    save_as = True


@admin.register(TextKeyboard)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('id', 'kb_key', 'kb_text',)
    list_display_links = ('id', 'kb_key', 'kb_text',)
    search_fields = ('id', 'kb_key', 'kb_text',)
    save_as = True
