from django.core.management.base import BaseCommand
from bot_to_do_list.bot import run_bot


class Command(BaseCommand):
    help = 'Starts the To-Do-List bot'

    def handle(self, *args, **options):
        run_bot()
