from django.core.management.base import BaseCommand

from bot_echo.bot import run_bot


class Command(BaseCommand):
    help = 'Run Telegram bot'

    def handle(self, *args, **options):
        run_bot()