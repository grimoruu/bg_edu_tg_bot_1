from django.core.management.base import BaseCommand
import subprocess


class Command(BaseCommand):
    help = 'Run Docker Compose'

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Running Docker Compose...'))
        try:
            subprocess.run(['docker', 'compose', 'up', '-d'])
        except Exception as e:
            self.stdout.write(self.style.ERROR(f'Error running Docker Compose: {e}'))
