import re

import telebot
from telebot.types import Message

from bot_to_do_list.config import BOT_TOKEN
from bot_to_do_list.core.dao import TextMessagesServiceDAO, StatesServiceDAO, HandleCommandsServiceDAO, UserServiceDAO
from bot_to_do_list.core.services import TaskService, UserTaskService
from bot_to_do_list.kb import menu_kb, confirm_task_addition_kb, confirm_task_update_kb, tasks_keyboard, tasks_list_kb

bot = telebot.TeleBot(BOT_TOKEN)


class MessageHandler:
    def __init__(self, message: Message):
        self._bot = bot
        self._user_id = message.from_user.id
        self._message_text = message.text
        self._text_message_dao = TextMessagesServiceDAO()
        self._task_service = TaskService()
        self._user_task_service = UserTaskService()
        self._states_service = StatesServiceDAO()
        self._commands_service = HandleCommandsServiceDAO()
        self._user = UserServiceDAO()

    def handle_message(self):
        commands = self._commands_service.select_commands()
        command_dict = {cmd.command_key: cmd.command_value for cmd in commands}
        msg = self._message_text.lstrip("/")  # чуть проще
        if msg in command_dict:
            function_name = command_dict[msg]
            function = getattr(self, function_name)
            function()
        else:
            self.handle_other()

    def handle_start(self):
        user = self._task_service.check_user_service(self._user_id)
        if user:
            msg = self._text_message_dao.select_text_message("WELCOME_BACK_MESSAGE")
            self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())
        else:
            self._user.create_user(self._user_id, 0, True)
            msg = self._text_message_dao.select_text_message("START_MESSAGE")
            self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())

    def handle_menu(self):
        msg = self._text_message_dao.select_text_message("SIMPLE_QUESTION")
        self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())

    def handle_other(self):
        state_data = self._user_task_service.get_tasks_service(self._user_id)
        if state_data:
            states = self._states_service.select_states()
            state_dict = {state.state_key: state.state_value for state in states}
            state = state_data.state
            new_state = state.rstrip("0123456789_")  # сомнительно. чуть проще
            if new_state in state_dict:
                function_name = state_dict[new_state]
                function = getattr(self, function_name)
                function(state)
        else:
            self.handle_warning()

    def handle_add_task_title(self, state):
        self._user_task_service.update_task_service(self._user_id, self._message_text, None, "ADD_TASK_DESCRIPTION")
        msg = self._text_message_dao.select_text_message("ADD_TASK_DESCRIPTION")
        self._bot.send_message(self._user_id, msg)

    def handle_add_task_description(self, state):
        self._user_task_service.update_task_service(self._user_id, None, self._message_text, "ADD_DATA")
        msg = self._text_message_dao.select_text_message("ADD_TASK_DESCRIPTION")
        self._bot.send_message(self._user_id, msg, reply_markup=confirm_task_addition_kb())

    def handle_enter_new_task_title(self, state):
        task_id = state.split("_")[4]
        self._user_task_service.update_task_service(self._user_id, self._message_text, None,
                                                    f"ENTER_NEW_TASK_DESC_{task_id}")
        msg = self._text_message_dao.select_text_message("ENTER_NEW_TASK_DESC")
        self._bot.send_message(self._user_id, msg)

    def handle_enter_new_task_desc(self, state):
        task_id = state.split("_")[4]
        self._user_task_service.update_task_service(self._user_id, None, self._message_text, f"TASK_ID_{task_id}")
        msg = self._text_message_dao.select_text_message("CONFIRM_TASK_UPDATE")
        self._bot.send_message(self._user_id, msg, reply_markup=confirm_task_update_kb())

    def handle_warning(self):
        msg = self._text_message_dao.select_text_message("GET_WARNING")
        self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())


class CallbackQueryHandler:
    def __init__(self, callback_query):
        self._bot = bot
        self._callback_data = callback_query.data
        self._user_id = callback_query.from_user.id
        self._text_message_dao = TextMessagesServiceDAO()
        self._task_service = TaskService()
        self._user_task_service = UserTaskService()
        self._states_service = StatesServiceDAO()
        self._commands_service = HandleCommandsServiceDAO()
        self._user = UserServiceDAO()

    def callback_query(self):
        commands = self._commands_service.select_commands()
        command_dict = {cmd.command_key: cmd.command_value for cmd in commands}
        msg = self._callback_data
        match = re.search(r'\d', msg)
        if match:
            new_msg = msg[:match.start()]
        else:
            new_msg = msg
        if new_msg in command_dict:
            function_name = command_dict[new_msg]
            function = getattr(self, function_name)
            function()

    def handle_add_task(self):
        msg = self._text_message_dao.select_text_message("ADD_TASK_TITLE")
        self._bot.send_message(self._user_id, msg)
        self._user_task_service.create_task_service(self._user_id, None, None, "ADD_TASK_TITLE")

    def handle_save_task(self):
        tasks_data = self._user_task_service.get_tasks_service(self._user_id)
        self._task_service.create_task_service(self._user_id, tasks_data.title, tasks_data.description)
        self._user_task_service.delete_task_service(self._user_id)
        msg = self._text_message_dao.select_text_message("TASK_ADDED_SUCCESSFULLY")
        self._user.add_count_added_tasks(self._user_id)
        self._bot.send_message(self._user_id, msg)
        msg = self._text_message_dao.select_text_message("SIMPLE_QUESTION")
        self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())

    def handle_cancel_task(self):
        self._user_task_service.delete_task_service(self._user_id)
        msg = self._text_message_dao.select_text_message("TASK_ADDITION_CANCELLED")
        self._bot.send_message(self._user_id, msg)
        msg = self._text_message_dao.select_text_message("SIMPLE_QUESTION")
        self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())

    def handle_tasks_list(self):
        tasks = self._task_service.get_tasks_service(self._user_id)
        if tasks:
            keyboard = tasks_keyboard(tasks)
            msg = self._text_message_dao.select_text_message("CHOOSE_TASK_TO_VIEW")
            self._bot.send_message(self._user_id, msg, reply_markup=keyboard)
        else:
            msg = self._text_message_dao.select_text_message("TASK_LIST_EMPTY")
            self._bot.send_message(self._user_id, msg)
            msg = self._text_message_dao.select_text_message("SIMPLE_QUESTION")
            self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())

    def handle_task(self):
        task_data = self._callback_data.split("_")
        task_id = int(task_data[1])
        task_title = task_data[2]
        task_description = task_data[3]
        msg_text = f"Название задачи: {task_title}\n" f"Описание задачи: {task_description}"
        self._bot.send_message(self._user_id, msg_text)
        keyboard = tasks_list_kb(task_id)
        msg = self._text_message_dao.select_text_message("WHAT_TO_DO_WITH_TASK")
        self._bot.send_message(self._user_id, msg, reply_markup=keyboard)

    def handle_delete_task(self):
        task_data = self._callback_data.split("_")
        task_id = int(task_data[2])
        self._task_service.delete_task_service(task_id)
        msg = self._text_message_dao.select_text_message("TASK_DELETED_SUCCESSFULLY")
        self._bot.send_message(self._user_id, msg)
        keyboard = menu_kb()
        msg = self._text_message_dao.select_text_message("SIMPLE_QUESTION")
        self._bot.send_message(self._user_id, msg, reply_markup=keyboard)

    def handle_edit_task(self):
        task_data = self._callback_data.split("_")
        task_id = int(task_data[2])
        msg = self._text_message_dao.select_text_message("ENTER_NEW_TASK_TITLE")
        self._bot.send_message(self._user_id, msg)
        self._user_task_service.create_task_service(self._user_id, None, None, f"ENTER_NEW_TASK_TITLE_{task_id}")

    def handle_save_updated_task(self):
        data = self._user_task_service.get_tasks_service(self._user_id)
        task_id = int(data.state.split("_")[2])
        self._task_service.update_task_service(task_id, data.title, data.description)
        self._user_task_service.delete_task_service(self._user_id)
        msg = self._text_message_dao.select_text_message("TASK_UPDATED_SUCCESSFULLY")
        self._bot.send_message(self._user_id, msg)
        msg = self._text_message_dao.select_text_message("SIMPLE_QUESTION")
        self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())

    def handle_cancel_updated_task(self):
        self._user_task_service.delete_task_service(self._user_id)
        msg = self._text_message_dao.select_text_message("TASK_UPDATED_CANCELLED")
        self._bot.send_message(self._user_id, msg)
        msg = self._text_message_dao.select_text_message("SIMPLE_QUESTION")
        self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())

    def handle_return_to_menu(self):
        msg = self._text_message_dao.select_text_message("SIMPLE_QUESTION")
        self._bot.send_message(self._user_id, msg, reply_markup=menu_kb())
