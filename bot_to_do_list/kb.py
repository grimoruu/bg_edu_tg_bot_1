from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton

from bot_to_do_list.core.dao import TextKeyboardServiceDAO

kb_service = TextKeyboardServiceDAO()


def menu_kb() -> InlineKeyboardMarkup:
    kb = InlineKeyboardMarkup(row_width=2)
    add_task_button = InlineKeyboardButton(text=kb_service.select_kb("add_task"), callback_data="add_task")
    tasks_list_button = InlineKeyboardButton(text=kb_service.select_kb("tasks_list"), callback_data="tasks_list")
    kb.add(add_task_button, tasks_list_button)
    return kb


def confirm_task_addition_kb() -> InlineKeyboardMarkup:
    kb = InlineKeyboardMarkup(row_width=2)
    save_button = InlineKeyboardButton(text=kb_service.select_kb("save"), callback_data="save_task")
    cancel_button = InlineKeyboardButton(text=kb_service.select_kb("cancel"), callback_data="cancel_task")
    kb.add(save_button, cancel_button)
    return kb


def confirm_task_update_kb() -> InlineKeyboardMarkup:
    kb = InlineKeyboardMarkup(row_width=2)
    yes_button = InlineKeyboardButton(text=kb_service.select_kb("yes"), callback_data="save_updated_task")
    no_button = InlineKeyboardButton(text=kb_service.select_kb("no"), callback_data="cancel_updated_task")
    kb.add(yes_button, no_button)
    return kb


def return_to_menu_kb():
    kb = InlineKeyboardMarkup()
    return_button = InlineKeyboardButton(text=kb_service.select_kb("return_to_menu"), callback_data="return_to_menu")
    kb.add(return_button)
    return kb


def tasks_list_kb(task_id: int) -> InlineKeyboardMarkup:
    kb = InlineKeyboardMarkup(row_width=3)
    delete_button = InlineKeyboardButton(text=kb_service.select_kb("delete_task"), callback_data=f"delete_task_{task_id}")
    edit_button = InlineKeyboardButton(text=kb_service.select_kb("edit_task"), callback_data=f"edit_task_{task_id}")
    back_button = InlineKeyboardButton(text=kb_service.select_kb("return_to_list"), callback_data="tasks_list")
    kb.add(delete_button, edit_button, back_button)
    return kb


def tasks_keyboard(tasks: list) -> InlineKeyboardMarkup:
    buttons = [InlineKeyboardButton(text=f"{task.title}",
                                    callback_data=f"task_{task.task_id}_{task.title}_{task.description}")
               for task in tasks]
    buttons.extend(return_to_menu_kb().keyboard[0])
    kb = InlineKeyboardMarkup().add(*buttons)
    return kb
