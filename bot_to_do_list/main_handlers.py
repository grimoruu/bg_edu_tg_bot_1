from telebot.types import CallbackQuery, Message

from bot_to_do_list.handlers.services import MessageHandler, bot, CallbackQueryHandler


@bot.message_handler()
def message_handler(message: Message):
    service = MessageHandler(message)
    service.handle_message()


@bot.callback_query_handler(func=lambda callback_query: True)
def callback_query_handler(callback_query: CallbackQuery):
    service = CallbackQueryHandler(callback_query)
    service.callback_query()
