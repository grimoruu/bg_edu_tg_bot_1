from bot_to_do_list.core.dao import TaskServiceDAO, UserTaskServiceDAO, UserServiceDAO
from bot_to_do_list.core.schemas import TasksSchemaResponse, UserTasksSchemaResponse


class TaskService:
    def __init__(self) -> None:
        self._user_dao = TaskServiceDAO()
        self._user = UserServiceDAO()

    def get_tasks_service(self, user_id: int) -> list[TasksSchemaResponse]:
        tasks = self._user_dao.select_tasks(user_id)
        result = [TasksSchemaResponse(task_id=row.id, title=row.title, description=row.description) for row in tasks]
        return result

    def create_task_service(self, user_id: int, title: str, description: str | None) -> None:
        self._user_dao.create_task(user_id, title, description)

    def update_task_service(self, task_id: int, title: str, description: str | None) -> None:
        self._user_dao.update_task(task_id, title, description)

    def delete_task_service(self, task_id: int) -> None:
        self._user_dao.delete_task(task_id)

    def check_user_service(self, user_id: int) -> bool:
        return self._user.check_user(user_id)


class UserTaskService:
    def __init__(self) -> None:
        self._user_dao = UserTaskServiceDAO()

    def get_tasks_service(self, user_id: int) -> UserTasksSchemaResponse:
        items = self._user_dao.select_task(user_id)
        result = [UserTasksSchemaResponse(user_id=row.user_id, title=row.title, description=row.description,
                                          state=row.state) for row in items]
        return result[0] if result else None

    def create_task_service(self, user_id: int, title: str | None, description: str | None, state: str | None) -> None:
        self._user_dao.create_task(user_id, title, description, state)

    def update_task_service(self, user_id: int, title: str | None, description: str | None, state: str | None) -> None:
        self._user_dao.update_task(user_id, title, description, state)

    def delete_task_service(self, user_id: int) -> None:
        self._user_dao.delete_task(user_id)
