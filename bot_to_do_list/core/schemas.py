from pydantic import BaseModel


class TasksSchemaResponse(BaseModel):
    task_id: int
    title: str
    description: str


class UserTasksSchemaResponse(BaseModel):
    user_id: int
    title: str | None
    description: str | None
    state: str | None
