from typing import List, Optional

from myapp.models import Task, UserTask, TextMessage, State, HandleCommand, TextKeyboard, User


class UserServiceDAO:

    @staticmethod
    def check_user(user_id: int) -> bool:
        return User.objects.filter(user_id=user_id).exists()

    @staticmethod
    def create_user(user_id: int, count_of_created_tasks: int, check_box: bool) -> None:
        User.objects.create(user_id=user_id, count_of_created_tasks=count_of_created_tasks, check_box=check_box)

    @staticmethod
    def add_count_added_tasks(user_id: int) -> None:
        count = User.objects.get(user_id=user_id).count_of_created_tasks + 1
        User.objects.filter(user_id=user_id).update(count_of_created_tasks=count)


class TaskServiceDAO:

    @staticmethod
    def create_task(user_id: int, title: str, description: Optional[str] = None) -> None:
        Task.objects.create(user_id=user_id, title=title, description=description)

    @staticmethod
    def update_task(task_id: int, title: str, description: Optional[str] = None) -> None:
        Task.objects.filter(id=task_id).update(title=title, description=description)

    @staticmethod
    def delete_task(task_id: int) -> None:
        Task.objects.filter(id=task_id).delete()

    @staticmethod
    def select_tasks(user_id: int) -> List[Task]:
        return Task.objects.filter(user_id=user_id)


class UserTaskServiceDAO:
    @staticmethod
    def check_user(user_id: int) -> bool:
        return UserTask.objects.filter(user_id=user_id).exists()

    @staticmethod
    def create_task(user_id: int, title: Optional[str] = None, description: Optional[str] = None,
                    state: str = None) -> None:
        UserTask.objects.create(user_id=user_id, title=title, description=description, state=state)

    @staticmethod
    def update_task(user_id: int, title: str, description: Optional[str] = None, state: str = None) -> None:
        update_data = {}
        if title is not None:
            update_data['title'] = title
        if description is not None:
            update_data['description'] = description
        update_data['state'] = state
        UserTask.objects.filter(user_id=user_id).update(**update_data)

    @staticmethod
    def delete_task(user_id: int) -> None:
        UserTask.objects.filter(user_id=user_id).delete()

    @staticmethod
    def select_task(user_id: int) -> List[UserTask]:
        return UserTask.objects.filter(user_id=user_id)


class TextMessagesServiceDAO:

    @staticmethod
    def select_text_message(message_key: str) -> str:
        text_message = TextMessage.objects.get(message_key=message_key)
        return text_message.message_text


class StatesServiceDAO:

    @staticmethod
    def select_states() -> list:
        return State.objects.all()


class HandleCommandsServiceDAO:

    @staticmethod
    def select_commands() -> list:
        return HandleCommand.objects.all()


class TextKeyboardServiceDAO:

    @staticmethod
    def select_kb(kb_key: str) -> str:
        return TextKeyboard.objects.get(kb_key=kb_key).kb_text
